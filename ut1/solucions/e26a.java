//26a. Algoritme que indique si un nombre introduït per teclat és primer.

public class e26a
{
  public static void main(String args[])
  {
    int a, meitat/*,c=0*/;// boolean exacta=false;
    System.out.print("Introduzca un número entero positiu para comprobar si es primo: ");
    a = Integer.parseInt(System.console().readLine());
    if (a < 0)
    	System.out.println("és negatiu");
    else	
	{    	
	    meitat = a/2;
	    for(int i = 2;  i <= meitat  ;i++)
	    	if ((a % i) == 0)
	    	{
	    		System.out.println("NO és primer");
	    		//System.exit(0);
	    		return;
	    	}
	    System.out.println("SI és primer");	
	}
      	
  }
}
