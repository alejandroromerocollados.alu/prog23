//Programa que accepta un número i mostra els seus valors meitat i doble (per a 4, mostrarà 2 i 8).
public class e3 {
        public static void main (String args[]){

                //Declara la variable en la que guardaremos el valor númerico.
                double num1;
                
                //Pedimos el valor númerico con el que vamos a trabajar y le asignamos el valor a num1.
                System.out.print("Indique el valor númerico: ");
                num1 = Double.parseDouble(System.console().readLine());

                //Saca los resultados que nos pide el ejercicio y los calcula a la vez.
                System.out.println("La mitad de " +num1+ " es " + (num1/2) + " y su doble es " +(num1*2));


        }
}
