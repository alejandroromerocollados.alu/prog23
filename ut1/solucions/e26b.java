//26b. Algoritme que indique si un nombre introduït per teclat és primer.

// No és la solució més eficient, però volem practicar amb una variable booleana

public class e26b
{
  public static void main(String args[])
  {
    int a, meitat; boolean exacta=false;
    System.out.print("Introduzca un número entero mayor que 1 para comprobar si es primo: ");
    a = Integer.parseInt(System.console().readLine());
    if (a > 1)
    {
	    meitat = a/2 + 1;
	    for(int i = 2; (exacta == false) && (i <= meitat) ;i++)
	    	//System.out.println(i);	// autodepuració, mostre la variable contadora per a saber quantes passades fa el bucle
	    	if ((a % i) == 0)
	    		exacta = true;

	     if (exacta )	// s'ha trobat una divisió exacta
	      System.out.println("El número NO es primo");
	    else
	      System.out.println("El número SI es primo");
	}
	else
		System.out.println("2 com a mínim");
 	
  }
}
