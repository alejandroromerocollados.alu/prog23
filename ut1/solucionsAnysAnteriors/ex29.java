//29. Algorisme que indique si un any, introduït per teclat, és de traspàs (en castellà, bisiesto) o no.
//Fet per Emilio José el vostre company preferit 😽
public class ex29 
{
    public static void main (String[] args)
    {
        int any;
        
        System.out.println("😽😽😽 Introdueix un any per a saber si és de traspàs o no: 😽😽😽");
        any = Integer.parseInt(System.console().readLine());
        if ((any % 4 == 0) && (any % 100 != 0) || (any % 400 == 0))
	    	System.out.println("🤭🤭🤭 L' any " + any + " és de traspàs 🤭🤭🤭");
        else
            System.out.println("😱😱😱 L' any " + any + " no és de traspàs 😱😱😱");
    }
}
