
public class ex30{
    public static void main(String args[]){
        int num, suma;

        System.out.println("Introduce números para sumar sus dígitos.");
        System.out.println("Introduce 0 para realizar la suma.");
        System.out.print("Numero: ");
        num=Integer.parseInt(System.console().readLine());  
        suma=0;

        while (num>0){
            if (num/10==0){
                suma=suma+(num%10);
            }else{
                suma=suma+(num%10);
                num=num/10;
                continue;
            }
		System.out.print("Número: ");    
		num=Integer.parseInt(System.console().readLine());                
        }
        System.out.println("La suma es: "+suma);
    }
}
