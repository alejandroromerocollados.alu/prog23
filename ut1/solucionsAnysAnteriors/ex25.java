

public class ex25{
    
    public static void main(String args[]){
	final int TAM = 5;
        double n, min;
        //S'inicialitza la variable contm a 0
        int contm=1;
        
        System.out.println("Introduix un número:  ");
        n=Double.parseDouble(System.console().readLine());
        //La primera temperatura introduïa se considerarà com la primera mínima del programa
        min=n;
        //Se define un bucle for con las 14 pasadas restantes del programa que pidan temperaturas
        for (int i=0; i < TAM-1; i++){
            System.out.println("Introduix un número:  ");
            n=Double.parseDouble(System.console().readLine());
            //Si la nueva temperatura es inferior a la mínima, se asignará el nuevo valor y se incrementará el contador de mínimas en una unidad
            if (n<min){
                min=n;
                contm++;
            }
        }  
        System.out.println("La mínima de les " + TAM + " temperatures és: "+min+" i han sigut "+contm+" mínimes");
    }
}
