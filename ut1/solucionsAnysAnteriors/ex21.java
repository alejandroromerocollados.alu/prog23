// Programa que accepta nombres enters majors o iguals, 0 fins a acabar amb un nombre negatiu, i indique finalment quants han sigut múltiples de 2, quants ho han sigut de 3 i quants ho són de 2 i de 3 al mateix temps (de 6 per).

public class ex21
{
    public static void main(String args[])
    {
        int n1,cont2,cont3,cont6;
        
        cont2=cont3=cont6=0;

        System.out.println("Introduix un número");
        while(true)
        {
            n1 = Integer.parseInt(System.console().readLine());
            if(n1<0)
                break;
            if((n1%2)==0)
            	cont2++;
            	//System.out.println("El número es múltiplo de 2");
            if(n1%3==0)
            	cont3++;
            	//System.out.println("El número es múltiplo de 3");
            if(n1%6==0)
            	cont6++;
            
        }
       System.out.println("Han sigut " + cont2 + " múltiples de 2, " + cont3 + " múltiples de 3 i " + cont6 + " múltiples de 6");
    }
}
