import java.util.*;

public class ex32
{
	public static void main (String[]args)
	{
		//Creamos la variable
		int n;
		Scanner teclado= new Scanner(System.in);
		//Le pedimos al usuario que introduzca un valor
		System.out.print("Introduce un valor : ");
		//Guardamos el valor introducido por el usuario en la variable
		n=teclado.nextInt();
		for (int f=1 ; f <= n ; f++ ) 
		{
			System.out.println();	// principi de linia
			for (int y = f ; y <= n ;y++ ) 
				System.out.print(y + " ");

			//Rellenaremos los espacios en blanco con "0"
			for (int e=1 ; e <= f-1 ; e++) 
				System.out.print('0'+" ");
		}

	}	
}

