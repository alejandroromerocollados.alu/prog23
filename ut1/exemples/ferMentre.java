// Primer exemple de do-while

public class ferMentre
{
    public static void main(String args[])
    {
        double num;

        do
        {
            System.out.println("Introduix un positiu:");
            num = Double.parseDouble(System.console().readLine());
        }   while (num < 0);    // punt i coma final
    }
}
