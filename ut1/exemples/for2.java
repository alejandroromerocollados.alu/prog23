// Segon exemple d'un bucle for: mostrarà els múltiples de 5 positius i inferiors a 100 però en ordre invers: 95 90 ... 5 0

public class for2
{
    public static void main(String args[])
    {
        int cont;
        for( cont = 95 ; cont >= 0 ; cont = cont - 5)    // cont -= 5
            System.out.println(cont);
        System.out.println("La variable contadora ha acabat el bucle amb el valor " + cont);
    }
}



