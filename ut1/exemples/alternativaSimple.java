// Exemple d'alternativa simple (if): programa que indicarà si el valor introduït és positiu

public class alternativaSimple
{
    public static void main(String args[])
    {
        double num;

        System.out.println("Introduïx un número:");
        num = Double.parseDouble(System.console().readLine());
        if ( num >= 0) // NO PUNT I COMA
            System.out.println("És positiu");

    }
}
