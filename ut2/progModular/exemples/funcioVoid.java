// Exemple de funció que no retorna cap valor (void)


/* Programa MODULAR que demane un enter i mostre en pantalla tants asterics com indique el valor introduït */

public class funcioVoid
{
	public static void main(String[] args) {
		int n;
		
		System.out.println("Introduïx un enter:");
		n = Integer.parseInt(System.console().readLine());
		// crida a la funció
		asteriscos(n);
		System.exit(0);
	}

	// definició de la funció VOID
	public static void asteriscos(int num)
	{
		for(int i=1; i<= num ; i++)
			System.out.print('*');
	}

}


