/* 2b. utilitza la classe Music de l'exercici anterior, afegint-li un tercer atribut: nombre de músics
 (aquest atribut s'incrementarà en una unitat per a cada músic nou que es cree). */
	
class Music
{
	private String nom;
	private String instrument;
	private static int numMusics;	// = 0
	
	public Music()	// constructor per defecte
	{
		nom = "pepe";
		instrument = "piano";
		numMusics++;
	}
	
	public Music(String n,String i)	// constructor general
	{
		nom = n;
		instrument = i;
		numMusics++;
	}
	
	public Music(Music m)
	{
		nom = m.nom;
		instrument = m.instrument;
		numMusics++;
	}
	
	public void setNom(String n) { nom = n; }
	public void setInstrument(String i) { instrument = i; }
	public String getNom() { return nom; }
	public String getInstrument() { return instrument; }
	// no faig setter perque el contador de músics no ha de canviar-se
	public static int getNumMusics(){ return numMusics; }
	
	
	public void mostraMusic()
	{
		System.out.println("El músic " + nom + " toca l'instrument " + instrument);	
	}
	
	public String toString()
	{
		return  nom + " plays " + instrument;
	}
}

public class e2b
{
	public static void main(String args[])
	{
		Music m1 = new Music();	// constructor per defecte
		Music m2 = new Music("John Coltrane","saxofó");	// constructor general
		Music m3 = new Music(m1);	// constructor de còpia
		
		m1.mostraMusic();
		m2.mostraMusic();
		m3.mostraMusic();
		
		m1.setNom("Jose");
		m2.setInstrument("saxo");
		m3.setInstrument("teclat electrònic");
		
		System.out.println(m1);		// equivalent a System.out.println(m1.toString());	
		System.out.println(m2);
		System.out.println(m3);
		
		// mostrar quants músics he creat
		System.out.println("He creat " + Music.getNumMusics() + " músics");
		
		Music m4 = new Music();	// constructor per defecte
		System.out.println("He creat " + Music.getNumMusics() + " músics");
	}
	
}	
	
