import java.util.Scanner;

public class usaReloj
{
    public static void main(String[] args) 
    {
    	Scanner ent = new Scanner(System.in);
    	
    	reloj r1=new reloj(23,59,59);
    	System.out.println("Introduix quants segons vols afegir:");
    	int segons = ent.nextInt();
    	
    	r1.sumaSegundos(segons);
    	r1.mostrarReloj();
    	
    	reloj r2=new reloj();	// constructor per defecte: 12:00:00
    	r2.mostrarReloj();
    	
    	reloj r3=new reloj(0,0,0);	
    	r3.mostrarReloj();
    	
    	
    }
}
