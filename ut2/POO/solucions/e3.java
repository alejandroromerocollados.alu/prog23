/* 3. Crea un programa de gestió de discos de música. Per a això, defineix una classe Disc, sabent que per a cada disc volem registrar el títol (String), grup o music (String), preu (per defecte, 15 euros però algun disc pot estar en oferta, rebaixant-lo un 20%). Afig a la classe els constructors i mètodes que consideres útils o necessaris.

 El programa ha de crear 3 discos, amb valors de títols i grups introduïts des de teclat per l'usuari. A més el programa posarà en oferta un únic disc, triat a l'atzar amb Math.random(). Finalment, mostrarà tots els discos amb els seus preus. */
 import java.util.Scanner;
 
 
 class Disc{

	private String titol;
	private String grupic;
	private double preu = 15;

	// CONSTRUCTOR PER DEFECTE
	public Disc(){
		titol = "Revolver";
		grupic = "The Beatles";
		//preu = 15;
	}
	// constructor GENERAL
	public Disc(String t, String gm){
		titol = t;
		grupic = gm;
	}
	
	public Disc(Disc d){
		titol = d.titol;
		grupic = d.grupic;
	}

	public void setTitol(String t) { titol = t; }
	public void setGrupic(String g) { grupic = g; }
	public String getTitol() { return titol; }
	public String getGrupic() { return grupic; }
	public double getPreu() { return preu; }
	
	public double ofertaDisc(){
		preu = preu - preu * 0.2;	// preu = 0.8*preu;
		return preu;
	}
	 
	public void mostraDisc(){
		System.out.println("\nTítol: " + titol + "\nGrup o Music: " + grupic + "\nPreu: " + preu + "€");		
	}
	
	// toSring
	public String toString()
	{
		return "\nTítol: " + titol + "\nGrup o Music: " + grupic + "\nPreu: " + preu + "€";
	}
	 
}

 public class e3{
	
	public static void main(String[] args) {
		int oferta;

		oferta = (int)(3 * Math.random()) + 1;
		Disc d1 = new Disc(/*"Rumba", "Estepa"*/);	// constructor per defecte
		Disc d2 = new Disc("USB", "AnaGucci");	// constructor general
		Disc d3 = new Disc(d2);	// constructor de còpia
		
		switch(oferta){
			case 1:
				d1.ofertaDisc();break;
			case 2:
				d2.ofertaDisc();break;
			case 3:
				d3.ofertaDisc();break;
/*			default:
				System.out.println("Error");*/
		}
		d1.mostraDisc();
		d2.mostraDisc();
		//d3.mostraDisc();
		System.out.println(d3);	// crida al toString()
		
		
	}
}
