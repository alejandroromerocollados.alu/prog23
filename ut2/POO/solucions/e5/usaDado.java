public class usaDado
{

	public static void main (String args[])
	{
		int tiradas;
		dado d1= new dado();	// constructor per defecte
		dado d2= new dado(5);	// constructor general
		dado d3= new dado(d2);	// constructor de còpia
		
		d1.muestraDado();
		d2.muestraDado();
		d3.muestraDado();
		
		//tirada2();
		tiradas = genera666(d2);
		System.out.println("Han fet falta " + tiradas + " per a generar 3 sisos consecutius");
		
		
	
	}
	public static int genera666(dado d)
	{  int cont2=0,cont=0,aleatorio;
		while(cont<3)
		{  
			// tire el dau
			d.tirada();
			// guarde en aleatori el valor de la tirada
			aleatorio=d.getValor();
			
			if (aleatorio==6)
				cont++;
			else
				cont=0;
			cont2++;
		}
		//System.out.println("Han salido 3 6 consecutivos despues de "+cont2+" tiradas.");
		return cont2;
	}


}
