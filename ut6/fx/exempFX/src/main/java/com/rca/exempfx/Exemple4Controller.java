package com.rca.exempfx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Exemple4Controller implements Initializable
{
    @FXML
    private Label lblNom;
    @FXML
    private TextField txtNom;
    @FXML
    private Label lblMail;
    @FXML
    private TextField txtMail;
    @FXML
    private Button btn;
    @FXML
    private void handleButtonAction(ActionEvent event)
    {
        String nom = txtNom.getText();
        String mail = txtMail.getText();
        System.out.println("S'introduirà: "+  nom + " - " + mail);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
// Required by Initializable interface
// Called to initialize a controller after
// the root element has been processed
    }
}