module com.rca.labeldemo {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;

    opens com.rca.exempfx to javafx.fxml;
    exports com.rca.exempfx;
}