// COMPTADOR DE CLICS
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class comptaClics
{
	static int cont = 0;
	public static void main(String args[])
	{
		
		JFrame jf = new JFrame("Comptador de clics");
		JPanel jp = new JPanel(new GridLayout(0,1));
		// afegiré el jp al jf
		jf.add(jp);
		jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		// per a centrar el JFrame en pantalla
		jf.setLocationRelativeTo(null);
		JLabel jl = new JLabel("Encara no has fet clic",SwingConstants.CENTER);
		JButton jb = new JButton("Fes me clic");
		// afegiré jl i jb al jp
		jp.add(jl);
		jp.add(jb);
		// dimensiona el JFrame en funció del seu contingut, SEMPRE DESPRÉS D'AFEGIR ELS COMPONENTS
		jf.pack();
		jf.setVisible(true);
		
		// CONTROL D'ESDEVENIMENT: FER CLIC AL BOTÓ
		// 1. cridem al mètode addXXXXXListener corresponent per al botó
		// 2. Li passem com a paràmetre un objecte que implemente la interfície ActionListener
		// 3. Això supossa implementar el mètode void actionPerformed​(ActionEvent e)
		// 4. al seu interior indiquem la resposta a l'esdeveniment
		jb.addActionListener(new ActionListener(){	// classe anònima que implementa ActionListener
			public void actionPerformed​(ActionEvent e)
			{
				// TODO write your own code here
				cont++;
				jl.setText("Has fet " +  cont + " clics");
			}
		});
	}
}
