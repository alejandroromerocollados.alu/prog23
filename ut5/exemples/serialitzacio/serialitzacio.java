// Exemple de seralització
import java.io.*;
import java.util.Scanner;

public class serialitzacio
{
	public static void main(String[] args) {
		Scanner ent = new Scanner(System.in);
		try(FileOutputStream fos = new FileOutputStream("usuaris.srz");
		ObjectOutputStream oos = new ObjectOutputStream(fos);)
		// Cree 3 objectes i els serialitze
		/*{
			Usuari u1 = new Usuari("Tiziano Barone","tizianobarone@dam1.com","tiziano");
			Usuari u2 = new Usuari("Daniel Ramírez","daniramirez@dam1.com","tiziano");
			Usuari u3 = new Usuari("Roberto Fernández","roberfernandez@dam1.com","tiziano");

			oos.writeObject(u1);
			oos.writeObject(u2);
			oos.writeObject(u3);
		}*/

	/* NO SERIA VÀLID UN UNIC OBJECTE		
		{	// prove a crear un únic objecte y escriure els 3 valors
			Usuari u1 = new Usuari("Tiziano Barone","tizianobarone@dam1.com","tiziano");
			oos.writeObject(u1);
			u1.setValors("Daniel Ramírez","daniramirez@dam1.com","tiziano");
			oos.writeObject(u1);
			u1.setValors("Roberto Fernández","roberfernandez@dam1.com","tiziano");
			oos.writeObject(u1);
		}*/
		{
			Usuari u=null;
			for(int i=1; i < 4 ; i++)
			{
				System.out.println("Introduix nom:");
				String nom = ent.nextLine();
				System.out.println("Introduix correu:");
				String email = ent.nextLine();
				System.out.println("Introduix contrasenya:");
				String passwd = ent.nextLine();
				u = new Usuari(nom,email,passwd);
				oos.writeObject(u);
			}
		
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage());
		}
	}
}
