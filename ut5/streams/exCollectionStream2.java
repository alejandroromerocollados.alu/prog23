import java.io.*;
import java.util.*;

abstract class figura
{
	private String color;
	public figura(String c) { color = c; }
	public abstract double area();
	public String getColor() { return color; }
}

class rectangle extends figura
{
	private double ample;
	private double alt;
	
	public rectangle(String c,double an,double al) { super(c); ample = an; alt = al; }
	public double area() { return ample*alt; }
}

public class exCollectionStream2
{
	public static void main(String args[])
	{
		Vector<figura> figures = new Vector<figura>();
		rectangle r1 = new rectangle("verd",2,3);
		rectangle r2 = new rectangle("verd",3,4);
		rectangle r3 = new rectangle("blau",10,4);
		figures.add(r1); figures.add(r2); figures.add(r3);

		double cont = figures.stream()
			.filter(w -> w.area() > 10)	// el predicat per a filter es que l'àrea de la figura ha de ser major que 10
			//.mapToDouble(w -> w.area())	// el toDoubleFunction és obtindre l'area a partir de la figura
			.count();
		System.out.println("Els rectangles d'àrea superior a 10 són " + cont);
	}
}	
