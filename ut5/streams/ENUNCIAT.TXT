Programa que genere 100 números de loteria (de 0 a 99999) i fent ús de fluxes mostre el màxim
    A utilitzar Arrays.streams(), max() 

Programa que genere 100 números de loteria (de 0 a 99999) i fent ús de fluxes mostre quants menors de 1000 s'han generat
    A utilitzar Arrays.streams(), filter() i count()

Programa que genere 100 números de loteria (de 0 a 99999) i fent ús de fluxes mostre la mitjana
    A utilitzar average() 

Programa que genere 100 números de loteria (de 0 a 99999) i fent ús de fluxes compte i mostre els valors truncats al miler inferior (0, 1000, 2000 ... fins a 99000 )
    A utilitzar map() i forEach() 
