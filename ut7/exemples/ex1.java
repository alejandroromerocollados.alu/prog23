// FER ÚS DE CLASSPATH (-cp) AL COMPILAR I EXECUTAR, AMB LA RUTA AL JAR DE MYSQL


import java.sql.*;

public class ex1 {
    
    public static void main(String[] args) /* throws SQLException*/ {

        Connection conn = null;
        //try ( Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/empresa","root","root");) {
        try ( Connection conexion = DriverManager.getConnection("jdbc:mysql://172.17.0.2/empresa","root","root");) {
            //REGISTRAR EL DRIVER: JA NO ÉS NECESSARI EN LES NOVES VERSIONS DEL JAR
            /* String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();*/
        
            // Establir la connexió amb la Base de Dades
            System.out.println("Connectant amb la base de dades...");

/*            String jdbcUrl = "jdbc:mysql://172.17.0.2:3306/empresa";
            Connection conexion = DriverManager.getConnection(jdbcUrl,"root","root");*/

            System.out.println("Connexió establida amb la base de dades...");
            // conexion.close();

       } catch(SQLException se) {
            se.printStackTrace();
        }
    }
    
}
