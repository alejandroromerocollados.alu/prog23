enum got
{

	JARRA(500), TUBO(250), TERCIO(333), CAÑA(200);
	private int cc; // Variable interna on guardem la capacitat

	got(int cc)
	{
		this.cc = cc;
	}

	public int getCentimetresCubics()
	{
		return cc;
	}
}


class BegudaCervesa
{

	enum MarcaCervesa {MAHOU, SANMIGUEL, HEINEKEN} // Tipos enumerados sencillos. Sólo tenemos estas marcas

	private got got;
	private MarcaCervesa marca;

	BegudaCervesa(MarcaCervesa marca, got got)
	{
		this.marca = marca;
		this.got = got;
	}

	public void servir()
	{
		System.out.println("Servint " + got + " de " + got.getCentimetresCubics() + "cc. de Cervesa " + marca);
	}
}

/*
 * Programa
 */
public class SolucionEjercicioEnums
{

	public static void main(String[] args)
	{
		BegudaCervesa birra = new BegudaCervesa(BegudaCervesa.MarcaCervesa.MAHOU,got.JARRA);

		birra.servir();
		
		BegudaCervesa tercio = new BegudaCervesa(BegudaCervesa.MarcaCervesa.HEINEKEN,got.TERCIO);

		tercio.servir();
	}
}
