/*
Realitza un programa que, mitjançant la classe Hashtable, permeta gestionar pel·lícules d'una videoteca o videoclub. Per a això, s'utilitzaran entrades de tipus <Integer,String> tal que cada pel·lícula tinga un número de pel·lícula i el seu títol.

	El programa permetrà:
	- introduir pel·lícules des de teclat
	- llistar totes les pel·lícules
	- eliminar una pel·lícula a partir del seu número
	- consultar el títol d'una pel·lícula a partir del seu número
*/

import java.util.Scanner;
import java.util.Hashtable;
import java.util.Enumeration;

public class exHashtable{

    private static int codigo = 100;   
    private static Scanner tec = new Scanner(System.in);
 
    public static void main(String args[]){
        
        Hashtable<Integer,String> peliculas = new Hashtable<Integer,String>(); //capacidad 11 por defecto

        //boolean programa = true; 
                
        while(true){
            System.out.println("\nMENU\n1 - Registrar película.\n2 - Lista de películas.\n3 - Eliminar película.\n4 - Buscar por código.\n0 - Salir.");
            int select = tec.nextInt();
            // descarte del buffer del teclat la polsació d'intro
            tec.nextLine();
            menu(peliculas,/*cod,tit,*/select);
        }
    }
    
    public static void menu(Hashtable<Integer,String> peliculas,/* Enumeration cod, Enumeration tit,*/ int select){
        switch(select){
            case 1: registro(peliculas); break;
            case 2: lista(peliculas/*,cod,tit*/); break;
            case 3: elimina(peliculas); break;
            case 4: busca(peliculas); break;
            case 0: System.exit(0);

        }
    }

    public static void registro(Hashtable<Integer,String> peliculas){
        System.out.println("Introdueix titol de la peli:");
        String titulo = tec.nextLine();
        peliculas.put(codigo,titulo);
        codigo++;
        System.out.println("\n--EXITO al registrar--");
    }

    public static void lista(Hashtable<Integer,String> peliculas/*, Enumeration cod, Enumeration tit*/){
        Enumeration<Integer> cod = peliculas.keys();
        Enumeration<String> tit = peliculas.elements();
        System.out.println("\nCOD -- TITULOS");
        while (cod.hasMoreElements()){
            System.out.print(cod.nextElement()+" -- ");
            System.out.println(tit.nextElement());
        }
    }
    
    public static void elimina(Hashtable<Integer,String> peliculas){
        System.out.println("Introdueix codi de la peli:");
         int c = tec.nextInt();
        peliculas.remove(c);
        System.out.println("\n--EXITO al eliminar--");
    }

    public static void busca(Hashtable<Integer,String> peliculas){
        System.out.print("\nIntroduce un código: ");
        int c = tec.nextInt();
        String s = peliculas.get(c);
        if (s == null)
            System.out.println("NO EXISTE");
        else
            System.out.println("Título: "+ s);
    }   
}
