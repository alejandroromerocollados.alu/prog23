/*1. Realitza un exercici (preferentment modular ) que permeta emmagatzemar noms (persones o ciutats, ..., la qual cosa preferisques) en un objecte Vector, amb mètodes que permeten carregar els seus valors de teclat, mostrar tots els noms per pantalla, etc. L'objecte Vector serà creat amb una capacitat inicial de 10 i increments de 5 en 5.
 El programa guardarà tots els noms que vulga introduir l'usuari fins a acabar amb una línia buida, mostrarà tots els noms emmagatzemats, així com la grandària i capacitat del vector, per a acabar demanant un nom que haurà d'eliminar del Vector. */

import java.util.Scanner;
import java.util.Vector;

public class exVector{
    public static void main(String[] args){
        Scanner tec = new Scanner(System.in);

        Vector<String> noms = new Vector<String>(10,5);

        introCity(noms,tec);

        muestraArray(noms);
        //System.out.println("Vector: "+noms);
        System.out.println("Grandària: "+noms.size());
        System.out.println("Capacitat: "+noms.capacity());
        
        System.out.print("Introduix el nom de la ciutat que vols esborrar: ");
        String ciutat = tec.nextLine();

/*        for(int i=0 ; i < noms.size() ; i++)
             //if(ciutat.compareTo(noms.get(i))==0)
             if (ciutat.equals(noms.get(i)))
                noms.remove(i);*/
        noms.remove(ciutat);

        /* for(String s : noms)
            if(ciutat.equals(s))
                noms.remove();*/

        System.out.println("Vector: " + noms);    
    }
    public static void introCity(Vector<String> noms,Scanner tec){
        //boolean programa = true;
        while(true){
            System.out.println("Polsa ENTER sense escriure res per a deixar d'introduïr ciutats.");
            System.out.print("Introduix el nom d'una ciutat: ");
            String ciutat = tec.nextLine();

            //if(ciutat.compareTo("")==0)
            if (ciutat.isEmpty())
                //programa=false;
                return;
            else
                noms.add(ciutat);
            
        }
    }
    public static void muestraArray(Vector<String> noms){
        for(String ciutat : noms)
            System.out.println(ciutat);
    }
}
