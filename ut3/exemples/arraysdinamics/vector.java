import java.util.*;

public class vector
{
	public static void main(String[] args)
	{
		Vector<String> v=new Vector<String>();  // capacitat de 10
		Scanner ent=new Scanner(System.in);
		String s="";
		
		System.out.println("Introduce un nombre:");
		s = ent.nextLine();

		while(!s.isEmpty())
		{
			v.add(s);   // afig el text al vector
			System.out.println("Introduce un nombre:");
			s=ent.nextLine();
		}
				
		String primer = v.firstElement();
		System.out.println("El primer element és " + primer);
/*		Iterator vItr = v.iterator(); 
		
		System.out.println("\nElementos en el vector:"); 
		while (vItr.hasNext()) 
			System.out.println(vItr.next() + " "); */
			
		/* Este tipo de bucle puede recorrer cualquier clase que implemente la interfaz Iterable<E>. */
		for (String nom: v) // bucle FOREACH, només es pot fer amb classes que implementen Iterable
			System.out.println(nom);
						
		System.out.println();
	}
}
