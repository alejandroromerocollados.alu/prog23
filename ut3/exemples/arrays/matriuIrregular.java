// Exemple de matriu IRREGULAR

import java.util.Scanner;
public class matriuIrregular
{
	static final int F=3,C=3;
	
	public static void main(String args[])
	{
		Scanner ent = new Scanner(System.in);
		
		double matriu[][] = new double[F][];	// matriu irregular: no indiquem nombre de columnes
		
		for(int i= 0; i < F ; i++)
			matriu[i] = new double[i+1];	// estic creant cada fila de la matriu
		
		// carregue valors des de teclat
		for(int i= 0; i < F ; i++)
			for (int j= 0; j <= i; j++)
			{
				System.out.println("Introduix un valor numèric:");
				matriu[i][j] = ent.nextDouble();
			}
		
		// mostrar valors de la matriu en pantalla
		for(int i= 0; i < F ; i++)
		{
			for (int j= 0; j <= i; j++)
				System.out.print(matriu[i][j] + " ");
			System.out.println("");
		}	
	}
}
