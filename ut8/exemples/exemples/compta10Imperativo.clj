; Programa que mostre de 1 a 10 a la manera de la programació imperativa

	(def n 1)
	(while (< n 11)
		(println n)
		(def n (inc n))
	); fi while
