/*3. Realitza un programa tal que:
a
• defineix la classe Ocell amb un atribut longitudBec, un constructor amb un paràmetre, setter,
getter, mètode pia() (que mostre en pantalla "PIO") i mètode abstracte vola().

• defineix la classe Colibri que herete de l'anterior i incloga un atribut color de tipus String, amb el
seu corresponent setter i getter, un constructor amb 2 paràmetres des del qual es cride al
constructor de la seua classe base, un constructor de còpia, i redefinisca o sobreescriga
(overriding) el mètode pia (un colibrí no fa "PIO", sinó "PIO, PIO", i ho fa cridant dues vegades al
mètode pia() de la seua classe base). A més, un colibrí vola a 30 Km/h (ho mostrarà per pantalla
en volar), i cova els seus ous (mostra per pantalla que està covant els seus ous).

• crea un objecte Colibri i fes-ho piular, volar i covar.

• crea un segon objecte Colibri, còpia de l'anterior.

¿Què canviaria si els objectes colibrí es crearen amb una referència a Ocell? Explica-ho en el propi
codi font del programa.*/

//clase base

package ocells;
public abstract  class Ocell
{
	protected double longitudBec;
	
	public Ocell (double lon) {longitudBec=lon;}

	public void setLogitudBec (double lon) {longitudBec=lon;}
	public double getLongitudBec () {return longitudBec;}

	public void pia (){System.out.println("PIO ");}
	public abstract void vola();


	public String toString () {return "Logitud del bec: "+longitudBec+" cms";}

}
