// Exemple de final


// Aplicat a una classe, la classe final serà el final de l'herència

// Aplicat a un mètode indicarà que no es podrà sobre-escriure en alguna classe derivada

// Aplicat a una variable de tipus bàsic significa que no podrà canviar el seu valor (serà constant)

// Aplicat a una referència significa que ...

/*final*/ class base
{
	private final int num;
	// private final rectangle r; 
	public /*final*/ void metode() {System.out.println("Hola");}
	
}

class derivada extends base
{
	@Override
	public void metode() {System.out.println("Hola en la classe derivada");}

}

public class Final
{
	public static void main(String args[])
	{
		base b = new base();
		derivada d = new derivada();
	
		b.metode();
		d.metode();
		
		base bd = new derivada();	// tipus estàtic és base i el tipus dinàmic és derivada
		bd.metode();
	}
	
}
