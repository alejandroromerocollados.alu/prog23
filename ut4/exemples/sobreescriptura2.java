// Exemple de sobreescriptura: el tipus d'objecte, i no la referència, determinen a quin mètode "hola()" estem cridant

class A {
	void hola() {
		System.out.println("Estic en A");
	}
}
class B extends A {
	void hola() {
		System.out.println("Estic en B");
	}
}
class C extends A {
	void hola() {
		System.out.println("Estic en C");
	}
}

class sobreescriptura2 {
	public static void main (String args []) {
		A a = new A();
		B b = new B();
		C c = new C();
		A r; // Obtenció d'una referència a un objecte A
		r = a;	
		r.hola();	// tipus estàtic: A, tipus dinàmic: A
		// Exemple d'upcasting (referència a la classe base, objecte de la classe derivada)
		r = b;
		r.hola();	// tipus estàtic: A, tipus dinàmic: B
		// Exemple d'upcasting (referència a la classe base, objecte de la classe derivada)
		r = c;
		r.hola();	// tipus estàtic: A, tipus dinàmic: C
	}
}

// Amb una referència a la classe base (línia 24) no tinc "visibilitat" del contingut de les classes derivades
